#include <string>
#ifndef OBSERVABLE_H
#define OBSERVABLE_H
#include"Observer.h"
using namespace std;
class Observable
{
public:
	virtual void addObserver(Observer*)=0;
	virtual void removeObserver(Observer*)=0;
	virtual void notifyObserver()=0;
};
#endif // !OBSERVABLE_H

