/**
*@mainpage OOB LAB HOMEWORK 6
*
*1. There are some informations of authors of the code on the "related pages".
*
*/
/**
* @page     : Introduction
* @author   : Halitcan Turan
* @author   : Yasin Binler
* @author   : Ogun Gungor
* @author   : Muhammet Emin Kurt
* @date     : 15 January 2021 
* @version  : v1.0
*/

/**
* These codes provide to read the file name from the command line with the help of argv.
*
* @param int argc
* @param char* argv[]
*/


#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <string.h>
#include "engine.h"
#include "FileOperation.h"

using namespace std;


int main(int argc, char* argv[])/// files taking from command line
{
	try
	{
		if (argc == 2)
		{
			string inputFilename = argv[1];
			string outputFilename = "output.txt";
			FileOperation file(inputFilename, outputFilename);
		}
		else if (argc == 3)
		{
			string inputFilename = argv[1];
			string outputFilename = argv[2];
			FileOperation file(inputFilename, outputFilename);
		}
		else if (argc == 1) throw std::exception(" Please enter a input file !!!!! ");
	}
	catch (exception const& ex) {
		cerr << "Exception: " << ex.what() << endl;
	} //end-catch
	system("pause");
}


