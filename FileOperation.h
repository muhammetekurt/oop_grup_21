#ifndef INPUT
#define INPUT

#include <iostream>
#include <vector> 
#include <string.h>
#include <string>
#include <fstream>
#include "engine.h"

using namespace std;

class FileOperation
{
private:
	ifstream File;
	static ofstream OutputFile;
	string fileName;
public:
	FileOperation(string, string);
	~FileOperation();

	void setFilename(string);
	string getFilename();

	void readInputs(string filename);
	int SpaceCounter(string str);
	void tokenize(std::string const& str, const char delim, vector<string>& out);

	static void writeMessage(std::string);
};
#endif