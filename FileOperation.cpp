#include "FileOperation.h"
#include <iostream>
#include "NoticeObservable.h"

void FileOperation::readInputs(string filename)
{
    int pos;
    string delimiter = ";";
    string line;
    vector<string> lines;
    ifstream inputFile;
    inputFile.open(filename);
    
    if (!inputFile)
    {
        cout << "Error opening the file" << endl;
    }
    while (getline(inputFile, line)) { 
        if (line.find(delimiter) != -1) { 
            pos = line.find(delimiter);
            lines.push_back(line.substr(0, pos));    
            line = line.substr(pos + delimiter.length());
        }
    } 
    
    Engine& e = Engine::Get();

    double doubleValue;
    string temp;
    string cmnd;
    for (int i = 0; i < lines.size(); i++)
    {
        temp = lines[i];
        vector<string> out;
        vector<string> output;

		if (SpaceCounter(temp) ==2)
		{
			tokenize(temp, ' ', out);
			cmnd = out[0];

			pos = temp.find(" ");
			string value = temp.substr(pos + 1);

			tokenize(value, ' ', output);
			value = output[0];
			doubleValue = atof(value.c_str());

			value = output[1];
			double doubleValue2 = atof(value.c_str());


			if (cmnd == "fill_tank")
			{
				e.fill_tank(doubleValue,doubleValue2);
			}
            else
            {
                FileOperation::writeMessage("Command is incorrect or missing\n");
            }
		}
        else if (SpaceCounter(temp) ==1)  
        {
            tokenize(temp, ' ', out);
            cmnd = out[0];

            pos = temp.find(" ");
            string value = temp.substr(pos + 1);
            tokenize(value, ';', output);
            value = output[0];

            ///Converting string value to the double value
            doubleValue = atof(value.c_str());

            if (doubleValue==0)
            {
                FileOperation::writeMessage("Parameter of function is missing\n");
                continue;
            }
           

            if (cmnd == "give_back_fuel")
            {
                e.give_back_fuel(doubleValue);
            }
			else if (cmnd == "print_tank_info")
			{
				e.print_tank_info(doubleValue-1);
			}
            else if (cmnd == "add_fuel_tank")
            {
                e.add_fuel_tank(doubleValue);
            }

            else if (cmnd == "remove_fuel_tank")
            {
                e.remove_fuel_tank(doubleValue);
            }

            else if (cmnd == "connect_fuel_tank_to_engine")
            {
                e.connect_fuel_tank_to_engine(doubleValue);
            }
            else if (cmnd == "disconnect_fuel_tank_from_engine")
            {
                e.disconnect_fuel_tank_from_engine(doubleValue);
            }
            else if (cmnd == "open_valve")
            {
                e.open_valve(doubleValue);
            }
            else if (cmnd == "close_valve")
            {
                e.close_valve(doubleValue);
            }
            else if (cmnd == "break_fuel_tank")
            {
                e.break_fuel_tank(doubleValue);
            }
            else if (cmnd == "repair_fuel_tank")
            {
                e.repair_fuel_tank(doubleValue);
            }
			else if (cmnd == "wait")
			{
				for (int i = 0; i < doubleValue; i++)
				{
                    e.absorb_fuel();
					e.consume_fuel();
				}
			}
            else
            {
                FileOperation::writeMessage("Command is incorrect or missing\n");
            }

        }
        else if (SpaceCounter(temp) == 0)
        {
            tokenize(temp, ';', out);
            cmnd = out[0];
            if (cmnd == "start_engine")
            {
                e.start_engine();
            }
            else if (cmnd == "stop_engine")
            {
                e.stop_engine();
                e.give_back_fuel();
            }
			else if (cmnd == "list_connected_tanks")
			{
				e.list_connected_tanks();
			}
			else if (cmnd == "print_fuel_tank_count")
			{
				e.getCountfTanks();
			}
			else if (cmnd == "print_total_fuel_quantity")
			{
				e.print_total_fuel_quantity();
			}
			else if (cmnd == "print_total_consumed_fuel_quantity")
			{
				e.print_total_consumed_fuel_quantity();
			}
            else if (cmnd == "list_fuel_tanks")
            {
                e.list_fuel_tanks();
            }
            else if (cmnd == "stop_simulation")
            {

                NoticeObservable observable;
                observable.addObserver(&e);
                for (int i = 0; i < e.getTanks().size(); i++)
                {
                    observable.addObserver(&(e.getTanks()[i]));
                    (e.getTanks()[i]).setObservable(&observable);
                }

                observable.notifyObserver();
                e.setObservable(&observable);
                for (int i = 0; i < e.getTanks().size(); i++)
                {
                    observable.removeObserver(&(e.getTanks()[i]));
                }
                e.removeObserver();
                break;
            }
            else
            {
                FileOperation::writeMessage("Command is incorrect or missing\n");
            }
        }
        if (cmnd!="stop_engine")e.absorb_fuel();
		if (cmnd != "wait")e.consume_fuel();
    }
    inputFile.close(); 
}

void FileOperation::tokenize(std::string const& str, const char delim,std::vector<std::string>& out)
{
    size_t start;
    size_t end = 0;

    while ((start = str.find_first_not_of(delim, end)) != std::string::npos)
    {
        end = str.find(delim, start);
        out.push_back(str.substr(start, end - start));
    }
}


int FileOperation::SpaceCounter(string str)
{
	int counter = 0;
    for (int i = 0; i < str.size(); i++)
    {
        if (str[i] == ' ')
        {
			counter++;
        }
    }
    return counter;
}
FileOperation::FileOperation(string input, string output)
{

    setFilename(input);

    this->File.open(this->fileName, std::ios::in);

    if (!File) { std::cout << "File failed to open\n"; }
    this->OutputFile.open(output, std::ios::out);
    this->OutputFile.close();

    this->OutputFile.open(output, std::ios::app);
    if (!OutputFile) { std::cout << "File failed to open\n"; }
    readInputs(input);
}

ofstream FileOperation::OutputFile;
void FileOperation::writeMessage(string message)
{
    OutputFile << message<<endl;
}
void FileOperation::setFilename(string fileName) {
    this->fileName = fileName;
}
string FileOperation::getFilename() {
    return this->fileName;
}
FileOperation::~FileOperation() {
    File.close();
}
