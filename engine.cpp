#include "engine.h"
#include <iostream>
#include<string>
#include "FileOperation.h"

Engine Engine::e_Instance;

Engine::Engine(const double fuel_per_second, double ConsumedFuel, bool status) {
	this->status = status;
	internalTank.set_capacity(55.0);
	internalTank.set_valve(true);
	internalTank.set_fuel_quantity(0.0);
}

/**
* This function returns the status of the engine.
*/
bool Engine::get_status() const {
	return status;
}
/**
* This function starts the engine.
*/
void Engine::start_engine() {
	string message = "";
	if (get_status() == false)
	{
		if (connectedTanks.size() > 0)
		{
			if (internalTank.get_fuel_quantity() > 5.5)
			{
				status = true;
				FileOperation::writeMessage("Started to engine");

			}
			else {
				FileOperation::writeMessage("Error there should be at least 5.5 liter fuel in internal tank");
			}
		}
		else {
			FileOperation::writeMessage("Error there should be at least 1 connected tank to the engine\n");
		}
	}
	else
	{
		FileOperation::writeMessage("Engine has already running\n");
	}
}
/**
* This function determine the consume fuel
*/
void Engine::set_ConsumedFuel()
{
	ConsumedFuel += 5.5;
}
/**
* This function print  the total consumed fuel
*/
void Engine::print_total_consumed_fuel_quantity()
{
	FileOperation::writeMessage("\nTuketilen yakit = " + to_string(ConsumedFuel) + "\n");
}
/**
* This function consume the fuel from internaltank.
*/
void Engine::consume_fuel() 
{
	if (Engine::get_status() == true && internalTank.get_fuel_quantity() > 5.5) {
		double oldFuel = internalTank.get_fuel_quantity();
		internalTank.set_fuel_quantity(oldFuel - 5.5);
		Engine::set_ConsumedFuel();
	}
}
/**
* This function absorb the fuel.
*/
void Engine::absorb_fuel() 
{

	if (connectedTanks.size() > 0  && internalTank.get_fuel_quantity()<20.0)
	{
		for (int i = 0; i < connectedTanks.size(); i++)
		{
			if (connectedTanks[i].get_valve()==true)
			{
				float bos_alan = internalTank.get_capacity() - internalTank.get_fuel_quantity();
				if (bos_alan == connectedTanks[i].get_fuel_quantity())
				{
					internalTank.set_fuel_quantity(bos_alan + internalTank.get_fuel_quantity());
					connectedTanks[i].set_fuel_quantity(connectedTanks[i].get_fuel_quantity() - bos_alan);
					tanks[i].set_fuel_quantity(tanks[i].get_fuel_quantity() - bos_alan);
				}
				else if (bos_alan > connectedTanks[i].get_fuel_quantity())
				{
					float internaltankyakit = internalTank.get_fuel_quantity();
					internalTank.set_fuel_quantity(connectedTanks[i].get_fuel_quantity() + internaltankyakit);
					connectedTanks[i].set_fuel_quantity(0.0);
					tanks[i].set_fuel_quantity(0.0);

				}
				else if (bos_alan < connectedTanks[i].get_fuel_quantity())
				{
					float internaltankyakit = internalTank.get_fuel_quantity();
					internalTank.set_fuel_quantity(55.0);
					connectedTanks[i].set_fuel_quantity(connectedTanks[i].get_fuel_quantity() - bos_alan);
					tanks[i].set_fuel_quantity(tanks[i].get_fuel_quantity() - bos_alan);

				}
			}
			if (internalTank.get_fuel_quantity() > 20)break;
		}
	}

}
/**
* This function absorb the fuel.
*/
void Engine::give_back_fuel() {
	string message;
	if (connectedTanks.size() > 0) {

		double min = connectedTanks[0].get_fuel_quantity();
		int id=0;
		for (int i = 0; i < connectedTanks.size(); i++)
		{
			if (min>connectedTanks[i].get_fuel_quantity())
			{
				id = i;
			}
		}
		double amount = connectedTanks[id].get_fuel_quantity() + internalTank.get_fuel_quantity();

		connectedTanks[id].set_fuel_quantity(amount);
		tanks[id].set_fuel_quantity(amount);
		internalTank.set_fuel_quantity(0.0);


	}
	else {
		FileOperation::writeMessage("There isn't any connected tank to engine\n");
	}
}
/**
* This function absorb the fuel.
* param double amount
*/
void Engine::give_back_fuel(double amount) {

	if (connectedTanks.size() > 0) {
		connectedTanks[0].set_fuel_quantity(connectedTanks[0].get_fuel_quantity() + amount);
	}
	else {
		FileOperation::writeMessage("There isn't any connected tank to engine\n");
	}
}

/**
* This function stop the engine.
*/
void Engine::stop_engine() {
	if (status==true)
	{
		status = false;
		FileOperation::writeMessage("Stopped the engine\n");
	}
	else
	{
		FileOperation::writeMessage("Engine has not already running\n");
	}
}
/**
* This function stop the program.
*/
void Engine::stop_simulation() {

	FileOperation::writeMessage("Stopped the simulation\n");
	system("pause");
	exit(0);
}

/**
* This function add a new tank.
* @param  double capacity
*/
void Engine::add_fuel_tank(double capacity) {
	Tank t(capacity);
	tanks.push_back(t);
	FileOperation::writeMessage("Added a new tank . Capacity :" + to_string(capacity) + "\n");
}
/**
* This function print the all tanks.
*/
void Engine::list_fuel_tanks() {
	string message="\nFUEL TANKS L�ST\n";

	for (int i = 0; i < tanks.size(); i++) 
	{
		message = message + "tank " + to_string(tanks[i].get_id())
			+ " capacity: " + to_string(tanks[i].get_capacity())
			+ " quantity: " + to_string(tanks[i].get_fuel_quantity())
			+ " broken: " + to_string(tanks[i].get_broken())
			+ " valve: " + to_string(tanks[i].get_valve()) + "\n";
	}
	FileOperation::writeMessage(message);
}
/**
* This function print the total fuel quantity.
*/
void Engine::print_total_fuel_quantity()
{
	string message;
	double totalFuel = 0.0;
	for (int i = 0; i < tanks.size(); i++)
	{
		totalFuel += tanks[i].get_fuel_quantity();
	}
	totalFuel += internalTank.get_fuel_quantity();
	FileOperation::writeMessage("Toplam yakit = " + to_string(totalFuel) + "\n");

}
/**
* This function print the  specific tanks' information.
* @param int id
*/
void Engine::print_tank_info(int id)
{
	string message = "TANK "+ to_string(id+1)+" INFO\n";

	message = message + "tank " + to_string(tanks[id].get_id())
		+ " capacity: " + to_string(tanks[id].get_capacity())
		+ " quantity: " + to_string(tanks[id].get_fuel_quantity())
		+ " broken: " + to_string(tanks[id].get_broken())
		+ " valve: " + to_string(tanks[id].get_valve()) + "\n";

	FileOperation::writeMessage(message);
}
/**
* This function fill the fuel of chosen tank.
* @param int id
* @param double fuel_quantity
*/
void Engine::fill_tank(int id, double fuel_quantity)
{
	string message ;
	float empty_amount = tanks[id - 1].get_capacity() - tanks[id - 1].get_fuel_quantity();
	if (empty_amount==0)
	{
		FileOperation::writeMessage("Tank " + to_string(id) + " is full\n");
	}
	else if (empty_amount<fuel_quantity)
	{
		tanks[id - 1].set_fuel_quantity(tanks[id - 1].get_fuel_quantity()+empty_amount);
		if(connectedTanks.size()>2) connectedTanks[id - 1].set_fuel_quantity(connectedTanks[id - 1].get_fuel_quantity() + empty_amount);

		FileOperation::writeMessage("Tank " + to_string(id) + " only " + to_string(empty_amount) + " liter fuel filled\n");
	}
	else if (empty_amount >= fuel_quantity)
	{
		tanks[id - 1].set_fuel_quantity(tanks[id - 1].get_fuel_quantity()+fuel_quantity);
		if (connectedTanks.size() > 2)connectedTanks[id - 1].set_fuel_quantity(connectedTanks[id - 1].get_fuel_quantity() + fuel_quantity);

		FileOperation::writeMessage("Tank " + to_string(id) + "  " + to_string(fuel_quantity) + " liter fuel filled\n");
	}
}
/**
* This function print the numbers of tanks.
*/
void Engine::getCountfTanks() {

	FileOperation::writeMessage("Number of tanks =" + to_string(tanks.size()) + "\n");
}
/**
* This function remove the chosen tank.
* @param int id
*/
void Engine::remove_fuel_tank(int id) 
{
	string message = "";
	for (int i = 0; i < tanks.size(); i++)
	{
		if (tanks[i].get_id() == id) {
			tanks.erase(tanks.begin() + i);
			message = "Removed the tank " + to_string(id)+"\n";
		}
	}
	if (message == "")
	{
		message = "Not found the tank "+ to_string(id)+"\n";
	}
	FileOperation::writeMessage(message);
}
/**
* This function break the chosen tank.
* @param int id
*/
void Engine::break_fuel_tank(int id)
{

	int flag = 0;
	for (int i = 0; i < tanks.size(); i++) {
		if (tanks[i].get_id() == id) {
			tanks[i].set_broken(true);
			FileOperation::writeMessage("Broke the tank " + to_string(id) + "\n");
			flag = 1;
		}
	}
	if (flag == 0)
	{
		FileOperation::writeMessage(" Not found the tank\n");

	}
}
/**
* This function repair the chosen tanks.
* @param int id
*/
void Engine::repair_fuel_tank(int id) 
{
	int flag = 0;
	for (int i = 0; i < tanks.size(); i++) {
		if (tanks[i].get_id() == id) {
			tanks[i].set_broken(false);
			FileOperation::writeMessage("Repaired the tank" + to_string(id) + "\n");
			flag = 1;
		}
	}
	if (flag == 0)
	{
		FileOperation::writeMessage(" Not found the tank "+to_string(id)+"\n");
	}
}
/**
* This function open the valve.
* @param int id
*/
void Engine::open_valve(int id)
{
	for (int i = 0; i < tanks.size(); i++) {
		if (tanks[i].get_id() == id){
			tanks[i].set_valve(true);
			FileOperation::writeMessage("Opened the valve " + to_string(id) + "\n");
		}
	}
	for (int i = 0; i < connectedTanks.size(); i++)
	{
		if (connectedTanks[i].get_id() == id) {
			connectedTanks[i].set_valve(true);
		}
	}
}
/**
* This function open the valve.
* @param int id
*/
void Engine::close_valve(int id) 
{
	string message;
	for (int i = 0; i < tanks.size(); i++) {
		if (tanks[i].get_id() == id) {
			tanks[i].set_valve(false);
			FileOperation::writeMessage("Closed the valve " + to_string(id) + "\n");
		}
	}
	for (int i = 0; i < connectedTanks.size(); i++)
	{
		
		if (connectedTanks[i].get_id() == id) {
			connectedTanks[i].set_valve(false);
		}
	}

}

/**
* This function connect fuel tank to engine.
* @param int id
*/
void Engine::connect_fuel_tank_to_engine(int id) 
{
	Engine &e = Engine::Get();				/// gets the only engine instance

	for (int i = 0; i < tanks.size(); i++)
	{
		if (tanks[i].get_id() == id && !tanks[i].get_broken())
		{ 
			e.connectedTanks.push_back(tanks[i]);
		}
	}

	bool Isthere = false;

	for (int i = 0; i < tanks.size(); i++)
	{
		if (tanks[i].get_id() == id) {
			Isthere = true;
		}
	}
	string message;
	if (Isthere == false)
	{
		message = "Tank not found!\n";
	}
	else
	{
		message = "Connected tank "+to_string(id)+" to engine\n";
	}
	FileOperation::writeMessage(message);
}
/**
* This function print connected tank to engine.
*/
void Engine::list_connected_tanks() {
	Engine& e = Engine::Get();		
	string message= "\nConnected tanks\n";
	for (int i = 0; i < e.connectedTanks.size(); i++)
	{
		message = message + to_string(e.connectedTanks[i].get_id())+"\n";
	}
	FileOperation::writeMessage(message);
}
/**
* This function disconnects fuel tank to engine.
* @param int id
*/
void Engine::disconnect_fuel_tank_from_engine(int id) {
	Engine& e = Engine::Get();
	int flag = 0;
	string message;
	for (int i = 0; i < tanks.size(); i++) {
		if (tanks[i].get_id() == id) {
			e.connectedTanks.erase(e.connectedTanks.begin() + i);
			//tanks[i].set_valve(false);                      ///close the valve
			flag = 1;
		}
	}
	if (flag == 0)
	{
		message = "Tank not found "+to_string(id)+"\n";
	}
	else {
		message = " Disconnected the tank " + to_string(id) + " from the engine\n";
	}
	FileOperation::writeMessage(message);
	
}
/**
* This function returns fuel tanks.
*/
vector<Tank>& Engine::getTanks()
{
	return tanks;

}
void Engine::notify(string message)
{
	FileOperation::writeMessage("Engine::" + message);
}
void Engine::removeObserver()
{
	observable->removeObserver(this);
}
void Engine::setObservable(Observable* obs)
{
	this->observable = obs;
}

