#ifndef ENGINE_H_
#define ENGINE_H_
#include "tank.h"
#include <vector>
#include<fstream>
#include<iostream>
#include"Observer.h"
#include "Observable.h"

using namespace std;
class Engine: public Observer
{
private:
	double fuel_per_second;
	double ConsumedFuel;
	bool status;
	Engine(double fuel_per_second = 5.5,double ConsumedFuel=0.0, bool status = false);///constructor is private singleton design pattern
	Tank internalTank;
	static Engine e_Instance;
	vector<Tank> tanks;
	vector<Tank> connectedTanks;
	Observable* observable;

public:
	static Engine& Get() {
		return e_Instance;
	}
	vector<Tank>& getTanks();
	Engine(const Engine&) = delete;

	bool get_status() const;

	void set_ConsumedFuel();
	void fill_tank(int, double);
	void consume_fuel();
	void print_total_consumed_fuel_quantity();

	void start_engine();
	void stop_engine();

	void absorb_fuel();
	void give_back_fuel(double);
	void give_back_fuel();

	void add_fuel_tank(double);
	void remove_fuel_tank(int);

	void list_fuel_tanks();
	void print_total_fuel_quantity();
	void print_tank_info(int);

	void break_fuel_tank(int);
	void repair_fuel_tank(int);

	void open_valve(int);
	void close_valve(int);

	void connect_fuel_tank_to_engine(int);
	void disconnect_fuel_tank_from_engine(int);
	void list_connected_tanks();

	void getCountfTanks();
	void stop_simulation();

	void notify(string);
	void removeObserver();
	void setObservable(Observable*);
};

#endif /* ENGINE_H_ */

