#include "tank.h"
#include "engine.h"
#include "FileOperation.h"
#include<iostream>
using namespace std;
int Tank::current_id = 0;

/**
* Constructor of the class.
*/
Tank::Tank(double capacity) : id(current_id++) {
	this->capacity = capacity;
	this->fuel_quantity = 0;
	broken = false;  
	valve = false;	 
}

/**
* This function return the tank's id.
*/
int Tank::get_id() const {
	return id;
}
/**
* This function return th tanks's capacity.
*/
double Tank::get_capacity() const {
	return capacity;
}
/**
* This function assign th tanks's capacity.
* @param double capacity
*/
void Tank::set_capacity(double capacity) {
	this->capacity = capacity;
}
/**
* This function return the situation of tanks's.
*/
bool Tank::get_broken() const {
	return broken;
}
/**
* This function assign the situation of tanks's.
* @param bool value
*/
void Tank::set_broken(bool value) {
	broken = value;
}
/**
* This function return the situation of valve's.
*/
bool Tank::get_valve() const {
	return valve;
}
/**
* This function assign the situation of valve's.
* @param bool isOpen
*/
void Tank::set_valve(bool isOpen) {
	valve = isOpen;
}
/**
* This function return the fuel of quantity.
*/
double Tank::get_fuel_quantity() const {
	return fuel_quantity;
}
/**
* This function assign the fuel of quantity.
* @param double quantity
*/
void Tank::set_fuel_quantity(double quantity) {
	fuel_quantity = quantity;
}
/**
* This function sends message.
* @param string message
*/

int Tank::index = 1;
void Tank::notify(string message)
{
	FileOperation::writeMessage("Tank(" + to_string(index) + ")" + "::" + message + "\n" +
		"Valve(" + to_string(index) + ")" + "::" + message);

	index++;

}
/**
* This function set observable.
* @param Observable* obs)
*/
void Tank::setObservable(Observable* obs)
{
	this->observable = obs;
}
/**
* This function remove observable.
*/
void Tank::removeObserver()
{
	observable->removeObserver(this);
}





