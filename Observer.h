#include <string>
#ifndef OBSERVER_H
#define OBSERVER_H
using namespace std;
class Observer
{
public:
	 virtual void notify(string m)=0;
};
#endif

