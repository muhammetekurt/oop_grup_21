#ifndef NOTICE_OBSERVABLE
#define NOTICE_OBSERVABLE
#include<vector>
#include"Observer.h"
#include"Observable.h"
class NoticeObservable:public Observable
{
private:
	vector<Observer*> obs;
	string message = "Simulation stopped";
public:
	void addObserver(Observer* observer);
	void removeObserver(Observer* observer);
	void notifyObserver();

};
#endif 
