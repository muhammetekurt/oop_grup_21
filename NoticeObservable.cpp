#include "NoticeObservable.h"

void NoticeObservable::addObserver(Observer* observer)
{
	obs.push_back(observer);
}

void NoticeObservable::removeObserver(Observer* observer)
{
	auto iter=obs.begin();
	if (find(iter, obs.end(), observer) == obs.end())
		throw("NoticeObservable::removeObserver The element cannot be found");
	else
		obs.erase(find(iter, obs.end(), observer));
}

void NoticeObservable::notifyObserver()
{
	for (int i = 0; i < obs.size(); i++)
		obs[i]->notify(message);
}
